meta:
  id: id_021__ptquebec__smart_rollup__address
  endian: be
doc: ! 'Encoding id: 021-PtQuebec.smart_rollup.address'
seq:
- id: smart_rollup_address
  size: 20
