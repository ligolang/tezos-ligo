meta:
  id: id_021__ptquebec__vote__ballots
  endian: be
doc: ! 'Encoding id: 021-PtQuebec.vote.ballots'
seq:
- id: yay
  type: s8be
- id: nay
  type: s8be
- id: pass
  type: s8be
