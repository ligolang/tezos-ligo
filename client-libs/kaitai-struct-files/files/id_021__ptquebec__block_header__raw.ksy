meta:
  id: id_021__ptquebec__block_header__raw
  endian: be
  imports:
  - block_header
doc: ! 'Encoding id: 021-PtQuebec.block_header.raw'
seq:
- id: id_021__ptquebec__block_header__raw
  type: block_header
