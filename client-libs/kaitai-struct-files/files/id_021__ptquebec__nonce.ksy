meta:
  id: id_021__ptquebec__nonce
  endian: be
doc: ! 'Encoding id: 021-PtQuebec.nonce'
seq:
- id: id_021__ptquebec__nonce
  size: 32
