meta:
  id: id_021__ptquebec__smart_rollup__kind
  endian: be
doc: ! 'Encoding id: 021-PtQuebec.smart_rollup.kind'
enums:
  id_021__ptquebec__smart_rollup__kind:
    0: arith
    1: wasm_2_0_0
    2: riscv
seq:
- id: id_021__ptquebec__smart_rollup__kind
  type: u1
  enum: id_021__ptquebec__smart_rollup__kind
