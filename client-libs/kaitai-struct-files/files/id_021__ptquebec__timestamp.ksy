meta:
  id: id_021__ptquebec__timestamp
  endian: be
  imports:
  - timestamp__protocol
doc: ! 'Encoding id: 021-PtQuebec.timestamp'
seq:
- id: id_021__ptquebec__timestamp
  type: timestamp__protocol
